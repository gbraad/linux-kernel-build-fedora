Linux Kernel - Build wrapper for Fedora
=======================================


This build wrapper performs a build of the kernel source as used by Fedora and will 
generate RPM packages.


Usage
-----

Download the [kernel-fedora.repo](kernel-fedora.repo) file and place it in `/etc/yum.repos.d`.

```
$ dnf install -y kernel  # or yum
```


Authors
-------

| [!["Gerard Braad"](http://gravatar.com/avatar/e466994eea3c2a1672564e45aca844d0.png?s=60)](http://gbraad.nl "Gerard Braad <me@gbraad.nl>") |
|---|
| [@gbraad](https://twitter.com/gbraad)  |

